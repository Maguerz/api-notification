const nodemailer = require('nodemailer');
const fs = require('fs');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'hashtagueulebatard@gmail.com',
        pass: 'motdepasse'
    }
});

let mailOptions = {
    from: 'hashtagueulebatard@gmail.com'
};

exports.sendMail = function (body) {
    let data;
    mailOptions.to = body.mail;
    switch (body.type) {
        case 'MDP_OUBLIE':
            mailOptions.subject = 'Votre demande de mot de passe oublié';
            data = fs.readFileSync('./templates/IFidelise-mdp-oublie/mdp-oublie.html', 'utf8');
            mailOptions.html = data.replace('[username]', body.username);
            mailOptions.attachments = [
                {
                    filename: 'instagram.png',
                    path: 'templates/IFidelise-mdp-oublie/images/instagram.png',
                    cid: 'instagram'
                },
                {
                    filename: 'logo-inline-shadow.png',
                    path: 'templates/IFidelise-mdp-oublie/images/logo-inline-shadow.png',
                    cid: 'logo-inline-shadow'
                },
                {
                    filename: 'password_icon_ok.jpg',
                    path: 'templates/IFidelise-mdp-oublie/images/password_icon_ok.jpg',
                    cid: 'password_icon_ok'
                },
                {
                    filename: 'twitter.png',
                    path: 'templates/IFidelise-mdp-oublie/images/twitter.png',
                    cid: 'twitter'
                }
            ];
            break;
        case 'PARTAGE_FIDELITE':
            mailOptions.subject = 'Votre demande de partage de fidelité';
            data = fs.readFileSync('./templates/IFidelise-partage-fidelite/partage-fidelite.html', 'utf8');
            mailOptions.html = data.replace('[username]', body.username);
            mailOptions.attachments = [
                {
                    filename: 'instagram.png',
                    path: 'templates/IFidelise-partage-fidelite/images/instagram.png',
                    cid: 'instagram'
                },
                {
                    filename: 'illo.png',
                    path: 'templates/IFidelise-partage-fidelite/images/illo.png',
                    cid: 'illo'
                },
                {
                    filename: 'twitter.png',
                    path: 'templates/IFidelise-partage-fidelite/images/twitter.png',
                    cid: 'twitter'
                },
                {
                    filename: 'logo-inline-shadow.png',
                    path: 'templates/IFidelise-partage-fidelite/images/logo-inline-shadow.png',
                    cid: 'logo-inline-shadow'
                }
            ];
            break;
        case 'UPGRADE_PLAN':
            mailOptions.subject = body.username + ', pourquoi ne pas améliorer votre expérience ?';
            data = fs.readFileSync('./templates/IFidelise-Upgradeplan/upgradeplan.html', 'utf8');
            mailOptions.html = data.replace('[username]', body.username);
            mailOptions.attachments = [
                {
                    filename: 'instagram.png',
                    path: 'templates/IFidelise-Upgradeplan/images/instagram.png',
                    cid: 'instagram'
                },
                {
                    filename: 'Higher_plans.png',
                    path: 'templates/IFidelise-Upgradeplan/images/Higher_plans.png',
                    cid: 'Higher_plans'
                },
                {
                    filename: 'twitter.png',
                    path: 'templates/IFidelise-Upgradeplan/images/twitter.png',
                    cid: 'twitter'
                },
                {
                    filename: 'logo-inline-shadow.png',
                    path: 'templates/IFidelise-Upgradeplan/images/logo-inline-shadow.png',
                    cid: 'logo-inline-shadow'
                }
            ];
            break;
        case 'INSCRIPTION_NEWSLETTER':
            mailOptions.subject = 'Merci de votre inscription !';
            data = fs.readFileSync('./templates/IFidelise-inscription-newsletter/inscription-newsletter.html', 'utf8');
            mailOptions.html = data.replace('[username]', body.username);
            mailOptions.attachments = [
                {
                    filename: 'instagram.png',
                    path: 'templates/IFidelise-inscription-newsletter/images/instagram.png',
                    cid: 'instagram'
                },
                {
                    filename: 'illo.png',
                    path: 'templates/IFidelise-inscription-newsletter/images/illo.png',
                    cid: 'illo'
                },
                {
                    filename: 'twitter.png',
                    path: 'templates/IFidelise-inscription-newsletter/images/twitter.png',
                    cid: 'twitter'
                },
                {
                    filename: 'logo-inline-shadow.png',
                    path: 'templates/IFidelise-inscription-newsletter/images/logo-inline-shadow.png',
                    cid: 'logo-inline-shadow'
                }
            ];
            break;
        default:
            return;
    }
    sendRealMail();
};

function sendRealMail() {
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}