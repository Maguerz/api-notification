const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mail = require('./services/mail');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../swagger.json');

const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.post('/mail', (req, res) => {
    mail.sendMail(req.body);
    res.sendStatus(200);
});

app.listen(8080, () =>
    console.log('Server listening on port 8080'),
);